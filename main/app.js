function distance(first, second){
	if((first === undefined && second === undefined)
			|| (first === null && second === null)) {
		return 0
	}
	if(!(Array.isArray(first) && Array.isArray(second))) {
		throw new TypeError("InvalidType")
	}
	let unique1 = first.filter((val, idx, self) => {
		return self.indexOf(val) === idx && second.indexOf(val) === -1
	})
	let unique2 = second.filter((val, idx, self) => {
		return self.indexOf(val) === idx && first.indexOf(val) === -1
	})
	
	return unique1.length + unique2.length
}


module.exports.distance = distance